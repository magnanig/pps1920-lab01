package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    private static final double FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    public void depositWithAtm(final int usrID, final double amount) {
        deposit(usrID, amount - FEE);
    }

    public void withdrawWithAtm(final int usrID, final double amount) {
        withdraw(usrID, amount + FEE);
    }
}
