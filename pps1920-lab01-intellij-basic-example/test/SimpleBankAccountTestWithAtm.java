import lab01.example.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleBankAccountTestWithAtm extends SimpleBankAccountTest {

    private AccountHolder accountHolder;
    private BankAccountWithAtm bankAccount;

    @BeforeEach
    void beforeEach() {
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccount = new SimpleBankAccountWithAtm(accountHolder, 0);
        init(accountHolder, bankAccount);
    }

    @Test
    void testDepositWithAtm() {
        bankAccount.depositWithAtm(accountHolder.getId(), 100);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void testWithDrawWithAtm() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdrawWithAtm(accountHolder.getId(), 50);
        assertEquals(49, bankAccount.getBalance());
    }
}
