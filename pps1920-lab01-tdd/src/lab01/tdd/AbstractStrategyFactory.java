package lab01.tdd;

public abstract class AbstractStrategyFactory {
    public abstract SelectStrategy getEvenStrategy();

    public abstract SelectStrategy getMultipleOfStrategy(final int n);

    public abstract SelectStrategy getEqualsStrategy(final int n);
}
