package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    private final List<Integer> list;
    private int position;

    public CircularListImpl(){
        this.list = new ArrayList<>();
        this.position = 0;
    }

    @Override
    public void add(int element) {
        list.add(element);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if(isEmpty()){
            return Optional.empty();
        }
        return getAndMoveNext();
    }

    @Override
    public Optional<Integer> previous() {
        if(isEmpty()){
            return Optional.empty();
        }
        position = (position - 1 + size()) % size();
        return Optional.of(list.get(position));
    }

    @Override
    public void reset() {
        if(!list.isEmpty()){
            list.set(position, list.get(0));
        }
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if(list.isEmpty()) {
            return Optional.empty();
        }
        while(!strategy.apply(list.get(position))){
            position = (position + 1) % size();
        }
        return getAndMoveNext();
    }

    private Optional<Integer> getAndMoveNext(){
        Optional<Integer> val = Optional.of(list.get(position));
        position = (position + 1) % size();
        return val;
    }
}
