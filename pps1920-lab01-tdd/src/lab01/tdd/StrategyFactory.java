package lab01.tdd;

public class StrategyFactory extends AbstractStrategyFactory {
    @Override
    public SelectStrategy getEvenStrategy() {
        return i -> i % 2 == 0;
    }

    @Override
    public SelectStrategy getMultipleOfStrategy(final int n) {
        return i -> i % n == 0;
    }

    @Override
    public SelectStrategy getEqualsStrategy(final int n) {
        return i -> i == n;
    }
}
