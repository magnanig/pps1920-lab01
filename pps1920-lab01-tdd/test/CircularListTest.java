import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList list;

    @BeforeEach
    void setUp() {
        list = new CircularListImpl();
    }

    @Test
    void checkEmptyList(){
        assertTrue(list.isEmpty());
    }

    @Test
    void testAdd(){
        list.add(1);
        assertFalse(list.isEmpty());
        assertEquals(1, list.size());
    }

    @Test
    void testNextInEmptyList(){
        assertEquals(Optional.empty(), list.next());
    }

    @Test
    void testPreviousInEmptyList(){
        assertEquals(Optional.empty(), list.previous());
    }

    @Test
    void testOneNextAfterAddOnce(){
        list.add(1);
        try {
            assertEquals(1, list.next().get());
        } catch (NoSuchElementException ex){
            fail();
        }
    }

    @Test
    void testOnePreviousAfterAddOnce(){
        list.add(1);
        try {
            assertEquals(1, list.previous().get());
        } catch (NoSuchElementException ex){
            fail();
        }
    }

    @Test
    void testNextToBeCircular(){
        initList(3);
        List<Integer> expected = Arrays.asList(1, 2, 3, 1, 2, 3);
        List<Integer> actual = new ArrayList<>();
        try {
            for(int i = 0; i < 2 * list.size(); i++) {
                actual.add(list.next().get());
            }
        } catch (NoSuchElementException ex){
            fail();
        }
        assertEquals(expected, actual);
    }

    @Test
    void testPreviousToBeCircular(){
        initList(3);
        List<Integer> expected = Arrays.asList(3, 2, 1, 3, 2, 1);
        List<Integer> actual = new ArrayList<>();
        try {
            for(int i = 0; i < 2 * list.size(); i++) {
                actual.add(list.previous().get());
            }
        } catch (NoSuchElementException ex){
            fail();
        }
        assertEquals(expected, actual);
    }

    @Test
    void testReset(){
        initList(2);
        list.next(); //return 1 and then move pointer to 2
        list.reset(); //replace 2 with the first element (1)
        try {
            assertEquals(1, list.next().get());
        } catch (NoSuchElementException ex){
            fail();
        }
    }

    @Test
    void testNextWithBasicStrategy(){
        initList(10);
        testNextWithStrategy(n -> n % 2 == 0, Arrays.asList(2, 4, 6, 8, 10));
    }

    @Test
    void testNextWithEvenStrategyFactory(){
        initList(10);
        testNextWithStrategy(new StrategyFactory().getEvenStrategy(), Arrays.asList(2, 4, 6, 8, 10));
    }

    @Test
    void testNextWithMultipleOfStrategyFactory(){
        initList(20);
        testNextWithStrategy(new StrategyFactory().getMultipleOfStrategy(5), Arrays.asList(5, 10, 15, 20));
    }

    @Test
    void testNextWithEqualstrategyFactory(){
        initList(10);
        testNextWithStrategy(new StrategyFactory().getEqualsStrategy(5), Collections.singletonList(5));
    }

    private void testNextWithStrategy(SelectStrategy selectStrategy, List<Integer> expected){
        List<Integer> actual = new ArrayList<>();
        try {
            for (int i = 0; i < expected.size(); i++) {
                actual.add(list.next(selectStrategy).get());
            }
        } catch (NoSuchElementException ex){
            fail();
        }
        assertEquals(expected, actual);
    }

    private void initList(int max){
        for(int i = 1; i <= max; i++){
            list.add(i);
        }
    }
}
